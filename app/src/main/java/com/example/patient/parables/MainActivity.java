package com.example.patient.parables;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.patient.parables.helpers.RecentParable;
import com.example.patient.parables.model.Book;
import com.example.patient.parables.parableProvider.ChapterParableProvider;

public class MainActivity extends AppCompatActivity {

    private static final String[] menuItems = { "Книга", "Продолжить чтение", "По темам" };
    private RecentParable recentParable;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Book.setContext(this);      // как быть с контекстом для загрузки ресурсов в хелперах?..

        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        recentParable = RecentParable.load(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.main_menu_item, menuItems);

        lvMain.setAdapter(adapter);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                onMenuItemClick(position);
            }
        });
    }

    private void onMenuItemClick(int position) {
        switch (position) {
            case 0:
                showBook();
                break;
            case 1:
                showContinueRead();
                break;
            case 2:
                showTopics();
                break;
        }
    }

    private void showBook() {
        Intent intent = new Intent(this, ContentsActivity.class);
        startActivity(intent);
    }

    private void showContinueRead() {
        Intent intent = new Intent(this, ChapterActivity.class);
        intent.putExtra(ChapterActivity.PARABLE_PROVIDER, ChapterParableProvider.PROVIDER_NAME);
        intent.putExtra(ChapterParableProvider.CHAPTER_INDEX_NAME, recentParable.chapterIndex);
        intent.putExtra(ChapterParableProvider.PARABLE_INDEX_NAME, recentParable.parableIndex);

        startActivity(intent);
    }

    private void showTopics() {
        Intent intent = new Intent(this, TopicsActivity.class);
        startActivity(intent);
    }
}
