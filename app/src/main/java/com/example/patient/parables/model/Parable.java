package com.example.patient.parables.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "Parable")
public class Parable implements Serializable {
    @Element
    public int index;
    @Element
    public String text;

    public Parable(@Element(name = "index") int index, @Element(name = "text") String text) {
        this.index = index;
        this.text = text;
    }
}
