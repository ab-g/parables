package com.example.patient.parables;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.patient.parables.parableProvider.TopicsParableProvider;
import com.example.patient.parables.topics.TopicsCollection;

import java.util.ArrayList;
import java.util.List;

public class TopicsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);

        TopicsCollection.setContext(this);

        List<String> menuTopics = new ArrayList<>();

        for (int i = 0; i < TopicsCollection.getInstance().Topics.size(); i++) {
            menuTopics.add(TopicsCollection.getInstance().Topics.get(i).Name);
        }

        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.main_menu_item, menuTopics );

        lvMain.setAdapter(adapter);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                openTopic(position);
            }
        });
    }

    private void openTopic(int topicIndex) {
        Intent intent = new Intent(this, ChapterActivity.class);
        intent.putExtra(ChapterActivity.PARABLE_PROVIDER, TopicsParableProvider.PROVIDER_NAME);
        intent.putExtra(TopicsParableProvider.TOPIC_INDEX_NAME, topicIndex);

        startActivity(intent);
    }
}
