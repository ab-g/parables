package com.example.patient.parables.parableProvider;

import android.os.Bundle;

import com.example.patient.parables.model.Book;
import com.example.patient.parables.model.Chapter;
import com.example.patient.parables.model.Parable;
import com.example.patient.parables.topics.ParableGroup;
import com.example.patient.parables.topics.Topic;
import com.example.patient.parables.topics.TopicsCollection;

import java.util.ArrayList;

public class TopicsParableProvider implements IParableProvider {
    public static final String PROVIDER_NAME = "topicsParableProvider";
    public static final String TOPIC_INDEX_NAME = "chapterIndex";
    public static final String PREV_PARABLE_INDEX_NAME = "prevParableIndex";

    public int topicIndex;
    public int prevParableIndex;
    public Topic topic;
    private Bundle bundle;

    public TopicsParableProvider(Bundle bundle) {
        this.bundle = bundle;

        readExtras();

        topic = TopicsCollection.getInstance().Topics.get(0);
    }

    private void readExtras() {
        topicIndex = bundle.getInt(TOPIC_INDEX_NAME, 0);
        prevParableIndex = bundle.getInt(PREV_PARABLE_INDEX_NAME, 0);
    }

    @Override
    public ArrayList<Parable> getList() {
        int parableGroupIndex = Math.min(prevParableIndex + 1, topic.ParableGroups.size()) - 1;

        ParableGroup parableGroup = topic.ParableGroups.get(parableGroupIndex);

        Chapter chapter = Book.getInstance().Chapters.get(parableGroup.chapter - 1);

        int[] bounds = parableGroup.getBounds();

        return new ArrayList<>(chapter.Parables.subList(bounds[0] - 1, bounds[1]));
    }

    @Override
    public int getCurrentParableIndex() {
        return -1;
    }

    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public boolean hasPrev() {
        return false;
    }

    @Override
    public void savePosition(int position) {
    }
}
