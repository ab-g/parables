package com.example.patient.parables.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class RecentParable {
    private static final String CHAPTER_INDEX_PARAM = "chapterIndex";
    private static final String PARABLE_INDEX_PARAM = "parableIndex";
    private static final String PARAMS_FILE = "recent";

    public int chapterIndex;
    public int parableIndex;

    public RecentParable(int chapterIndex, int parableIndex) {
        this.chapterIndex = chapterIndex;
        this.parableIndex = parableIndex;
    }

    public static RecentParable load(Activity context) {
        SharedPreferences sPref = context.getSharedPreferences(PARAMS_FILE, Activity.MODE_PRIVATE);

        int chapterIndex = sPref.getInt(CHAPTER_INDEX_PARAM, 1);
        int parableIndex = sPref.getInt(PARABLE_INDEX_PARAM, 1);

        return new RecentParable(chapterIndex, parableIndex);
    }

    public static void Save(Context context, int chapterIndex, int parableIndex) {
        SharedPreferences sPref = context.getSharedPreferences(PARAMS_FILE, Activity.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();

        ed.putInt(CHAPTER_INDEX_PARAM, chapterIndex);
        ed.putInt(PARABLE_INDEX_PARAM, parableIndex);

        ed.apply();
    }
}
