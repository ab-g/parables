package com.example.patient.parables.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;

@Root(name = "Chapter")
public class Chapter implements Serializable {
    @Element
    public int Index;

    @ElementList
    public ArrayList<Parable> Parables = new ArrayList<>();
}
