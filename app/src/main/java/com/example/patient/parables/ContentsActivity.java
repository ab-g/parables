package com.example.patient.parables;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.patient.parables.model.Book;
import com.example.patient.parables.parableProvider.ChapterParableProvider;

import java.util.ArrayList;
import java.util.List;

public class ContentsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contents);

        List<String> menuItems = new ArrayList<>();
        for (int i = 1; i <= Book.getInstance().Chapters.size(); i++) {
            menuItems.add(String.format("Глава %d", i));
        }

        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.main_menu_item, menuItems );

        lvMain.setAdapter(adapter);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                openChapter(position + 1);
            }
        });
    }

    private void openChapter(int chapterIndex) {
        Intent intent = new Intent(this, ChapterActivity.class);
        intent.putExtra(ChapterParableProvider.CHAPTER_INDEX_NAME, chapterIndex);
        intent.putExtra(ChapterActivity.PARABLE_PROVIDER, ChapterParableProvider.PROVIDER_NAME);
        startActivity(intent);
    }
}
