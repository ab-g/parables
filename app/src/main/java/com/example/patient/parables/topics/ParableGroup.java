package com.example.patient.parables.topics;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "ParableGroup")
public class ParableGroup {
    @Attribute(name = "chapter")
    public int chapter;

    @Attribute(name = "verses")
    public String verses;

    public int[] getBounds() {
        String bounds[] = verses.split("-");
        // array.map(ToInt)...
        return new int[] { Integer.parseInt(bounds[0]), Integer.parseInt(bounds[1]) };
    }
}
