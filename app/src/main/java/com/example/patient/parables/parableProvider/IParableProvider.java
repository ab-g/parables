package com.example.patient.parables.parableProvider;

import com.example.patient.parables.model.Parable;

import java.io.Serializable;
import java.util.ArrayList;

public interface IParableProvider extends Serializable {
    ArrayList<Parable> getList();

    int getCurrentParableIndex();

    boolean hasNext();

    boolean hasPrev();

    void savePosition(int position);
}
