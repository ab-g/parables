package com.example.patient.parables.parableProvider;

import android.content.Context;
import android.os.Bundle;

import com.example.patient.parables.helpers.RecentParable;
import com.example.patient.parables.model.Book;
import com.example.patient.parables.model.Parable;

import java.util.ArrayList;

public class ChapterParableProvider implements IParableProvider {
    public static final String PROVIDER_NAME = "chapterParableProvider";
    public static final String CHAPTER_INDEX_NAME = "chapterIndex";
    public static final String PARABLE_INDEX_NAME = "parableIndex";

    public int chapterIndex;
    public int parableIndex;
    private Context context;
    private Bundle bundle;

    public ChapterParableProvider(Context context, Bundle bundle) {
        this.context = context;
        this.bundle = bundle;

        readExtras();
    }

    private void readExtras() {
        chapterIndex = bundle.getInt(CHAPTER_INDEX_NAME, 1);
        parableIndex = bundle.getInt(PARABLE_INDEX_NAME, 1) - 1;
    }

    @Override
    public ArrayList<Parable> getList() {
        return (Book.getInstance().Chapters.get(chapterIndex - 1)).Parables;
    }

    @Override
    public int getCurrentParableIndex() {
        return parableIndex;
    }

    @Override
    public boolean hasNext() {
        return chapterIndex < Book.getInstance().Chapters.size();
    }

    @Override
    public boolean hasPrev() {
        return chapterIndex > 1;
    }

    @Override
    public void savePosition(int position) {
        RecentParable.Save(context, chapterIndex, position);
    }
}
