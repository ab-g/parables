package com.example.patient.parables.topics;

import android.content.Context;

import com.example.patient.parables.R;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.ArrayList;

@Root(name = "TopicsCollection ")
public class TopicsCollection {
    @ElementList(name = "Topics")
    public ArrayList<Topic> Topics;

    private static Context context;
    private static TopicsCollection topicsCollection;

    public static void setContext(Context activityContext) {
        context = activityContext;
    }

    public static TopicsCollection getInstance() {
        if (topicsCollection == null) {
            topicsCollection = TopicsCollection.load();
        }

        return topicsCollection;
    };

    private static TopicsCollection load() {
        try
        {
            InputStream inputStream = context.getResources().openRawResource(R.raw.topics);

            Persister serializer = new Persister();

            topicsCollection = serializer.read(TopicsCollection.class, inputStream, false);
        } catch (Exception ignored) {
        }

        return topicsCollection;
    }

    // TODO: как-то вынести в дженерик вместе с Book
}
