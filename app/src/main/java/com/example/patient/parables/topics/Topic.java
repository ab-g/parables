package com.example.patient.parables.topics;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "Topic")
public class Topic {
    @ElementList(name = "ParableGroups")
    public ArrayList<ParableGroup> ParableGroups;

    @Attribute(name = "name")
    public String Name;

    @Attribute(name = "parablesCount")
    public int ParablesCount;
}
