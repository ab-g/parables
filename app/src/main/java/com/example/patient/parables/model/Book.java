package com.example.patient.parables.model;

import android.content.Context;

import com.example.patient.parables.R;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.ArrayList;

@Root(name = "Book")
public class Book {
    private static Context context;
    private static Book book;

    @ElementList
    public ArrayList<Chapter> Chapters = new ArrayList<>();

    public static void setContext(Context activityContext) {
        context = activityContext;
    }

    public static Book getInstance() {
        if (book == null) {
            book = Book.load();
        }

        return book;
    };

    private static Book load() {
        try
        {
            InputStream inputStream = context.getResources().openRawResource(R.raw.book);

            Persister serializer = new Persister();

            book = serializer.read(Book.class, inputStream, false);
        } catch (Exception ignored) {
        }

        return book;
    }
}
