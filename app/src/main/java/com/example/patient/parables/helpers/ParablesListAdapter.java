package com.example.patient.parables.helpers;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.patient.parables.R;
import com.example.patient.parables.model.Parable;

import java.util.ArrayList;

public class ParablesListAdapter extends ArrayAdapter<Parable> {
    public ParablesListAdapter(Context context, ArrayList<Parable> parables) {
        super(context, 0, parables);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Parable parable = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.parable_item, parent, false);
        }

        TextView tvText = (TextView) convertView.findViewById(R.id.parable_text);

        String htmlText = ParableItemContent(parable);
        tvText.setText(Html.fromHtml(htmlText));

        return convertView;
    }

    private String ParableItemContent(Parable parable) {
        return String.format("&nbsp;<font color=\"#496847\">%d</font>&nbsp;%s",
                parable.index, parable.text);
    }
}
