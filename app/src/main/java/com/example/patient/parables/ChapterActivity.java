package com.example.patient.parables;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.patient.parables.helpers.ParablesListAdapter;
import com.example.patient.parables.model.Parable;
import com.example.patient.parables.parableProvider.ChapterParableProvider;
import com.example.patient.parables.parableProvider.IParableProvider;
import com.example.patient.parables.parableProvider.TopicsParableProvider;

import java.util.ArrayList;

public class ChapterActivity extends AppCompatActivity {
    public final static String PARABLE_PROVIDER = "parableProviderName";

    private ListView lvMain;
    private IParableProvider parableProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);

        parableProvider = getParablesProviderByName(getIntent()
                .getStringExtra(ChapterActivity.PARABLE_PROVIDER));

        lvMain = (ListView) findViewById(R.id.lvMain);

        ArrayList<Parable> parables = parableProvider.getList();

        ParablesListAdapter adapter = new ParablesListAdapter(this, parables);

        lvMain.setAdapter(adapter);

        lvMain.setSelection(parableProvider.getCurrentParableIndex());
    }

    @Override
    protected void onPause() {
        super.onPause();

        int parableIndex = lvMain.getFirstVisiblePosition() + 1;
        parableProvider.savePosition(parableIndex);
    }

    private IParableProvider getParablesProviderByName(String providerName) {
        switch (providerName) {
            case ChapterParableProvider.PROVIDER_NAME:
                return new ChapterParableProvider(this, getIntent().getExtras());

            case TopicsParableProvider.PROVIDER_NAME:
                return new TopicsParableProvider(getIntent().getExtras());
        }

        return new ChapterParableProvider(this, getIntent().getExtras());
    }
}
